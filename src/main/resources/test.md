# Assessment Java 101

Welcome to the willhaben java assessment. To solve the problems ahead you can use
any frameworks, libraries, etc. you want, as long as your code is written in Java.

You can use any external (online) ressources as long as you can explain your solution
(e.g. [stackoverflow](https://stackoverflow.com/) is allowed).

The assessment consists of several parts, each depending on the previous one, so you
have to solve them in order.

Once you have finished the last part, please send your the source code
to your instructor.

## Requirements

* use one of the following build tools: `maven` or `gradle`
* your project should have the standard source code layout and work with the standard build command of your build tool
* adhere to the same coding quality standards as in a real production project
* design meaningful interfaces for your classes
* bonus points for tests ;-)

## Part 1 - Link extractor

Write a simple parser that, given a text file, extracts all links.

Links have the following characteristics:
* start either with `http://` or `https://` (so just `willhaben.at` is not a valid link, but `http://willhaben.at/` or [http://example.com] are).
* the part after `http://` or `https://` contains only the following characters: letters, numbers and any of the following characters: `+&@#/%?=~\-_|!:,.;`

## Part 2 - Link validator

Write a validation class that checks the following for each URL:

* the String is a valid URI (see (https://www.ietf.org/rfc/rfc2396.txt , [https://tools.ietf.org/rfc/rfc3986.txt] or
                                 https://en.wikipedia.org/wiki/Uniform_Resource_Identifier#cite_note-FOOTNOTERFC_23961998-4 )
* there are no port, query or fragment parameters
* there is no username or password information

e.g. the following URLs are invalid:
* https://me:secret@test.com/wh-assessment-101/invalid.txt
* https://test.com/wh/fragment#invalid
* https://test.com/wh-101/query?invalid=true
* https://test.com:8080/wh-101

the following URLs are valid:
* https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force
* https://tech.willhaben.at/

Again, check https://en.wikipedia.org/wiki/Uniform_Resource_Identifier for definitions of host, path, port, query, fragment, etc.

Select an interface from java.util.function that matches your validator best and implement it.

## Part 3 - Link processor

Write a service that given a list of links

* fetches the data with HTTP GET
* compute the MD5 checksum of the returned content
* uses all available cpu cores
* uses at most 10kB of memory for the fecthed data
* if an network error occurs, try one more time and then abort

## Part 4

Combine the previous parts into a command line application (with javadoc and help for the options and input).

Feed it this document (README.md) and answer the following question:

Which links have the the Base64 MD5 checksum `RbwRYKKAw0uSMwmukf8oOg==` and `5zAjHAcCDH/HsNXfEoVeMA==` ?

## Text with links

Taubenpost im Altertum
Die Taubenpost ist die älteste Form der [Flugpost](https://de.wikipedia.org/wiki/Flugpost). Bereits im Altertum erkannten die Menschen die besondere Fähigkeit von Tauben, mühelos aus größter Entfernung zu ihren Nistplätzen zurückzukehren. Dies erlaubt den Tauben in einem sehr großen Gebiet nach geeigneter Nahrung zu suchen. Um 5000 v. Chr. begann der Mensch mit der Domestikation der Taube. Durch verschiedenste Zuchtmethoden wurde es schließlich möglich, Tauben als Überbringer von Nachrichten einzusetzen, und sie gewannen immer mehr an wirtschaftlicher, militärischer und politischer Bedeutung.

Die ersten größeren Versuche zur der Taube unternahmen die Sumerer. Sargon von Akkad ließ alle seine Boten in Mesopotamien mit Tauben ausstatten, die im Falle eines Angriffs freigelassen werden sollten. Dies gewährleistete, dass der Herrscher schnell von einem Vorfall in Kenntnis gesetzt wurde.

Auch im Alten [Ägypten](https://de.wikipedia.org/wiki/Altes_%C3%84gypten) wurden Tauben zur schnellen Nachrichtenübermittlung eingesetzt. Freigelassene Tauben verkündeten beispielsweise die Kunde von der Krönung des Pharaos Ramses II. im Jahre 1279 v. Chr. Die oft in der postgeschichtlichen Literatur behauptete Verwendung von Tauben zur Briefbeförderung ist dagegen historisch nicht belegt. Zwar wurden bei der Krönung eines Pharaos oder beim Minfest vier freigelassene Tauben als Boten ausgesandt, was man aber noch nicht als geregelte Taubenpost bezeichnen konnte. Die eigentliche Taubenpost wurde wahrscheinlich erst in römischer oder frühislamischer Zeit in Ägypten eingeführt.[1]

Plinius der Ältere berichtete erstmals ausführlich über die Verwendung der Taubenpost
Tauben als Übermittler von Nachrichten wurden bald auch in anderen Hochkulturen eingesetzt. Eine erste Beschreibung der Taubenzucht lieferte der griechische Naturforscher und Philosoph
[Aristoteles](https://de.wikipedia.org/wiki/Aristoteles). Biologen nehmen an, dass die Brieftaube ursprünglich von der Felsentaube (Columba livia) abstammt.

Im antiken Griechenland erwiesen sich Tauben wegen der geografischen Beschaffenheit des Landes als ideales Mittel der Nachrichtenübermittlung, da viele wichtige Flugstrecken innerhalb der Reichweite der Tauben lagen. Athleten, die zu den Olympischen Spielen reisten, nahmen beispielsweise ihre eigenen Brieftauben mit. Im Falle eines Siegs banden die Athleten einen Teil des Zielbandes an den Fuß der Tauben, die anschließend zurück in die Heimat des Sportlers flogen und so den Einwohnern den Sieg ihres Mitbürgers signalisierten. Der römische Schriftsteller Claudius Aelianus berichtet in seiner Varia historia (9,2), dass der Grieche Taurosthenes auf diese Weise seinem Vater und seinem Heimatdorf auf der Insel Aigina die Nachricht von seinem Sieg bei den Olympischen Spielen überbrachte.

Im antiken Rom hatte die Brieftaube vor allem militärische Bedeutung. Der römische Feldherr Julius Caesar ließ Nachrichten von Unruhen im eroberten Gallien durch eigene Botentauben überbringen, um so seine Truppen schnell befehligen zu können. Der römische Senator und Schriftsteller Plinius der Ältere berichtete erstmals ausführlich in seinem naturwissenschaftlichen Werk Naturalis historia über die militärische Verwendung von Brieftauben. Er beschrieb nachträglich, wie Brutus während der Belagerung von Modena im Jahre 44 v. Chr. durch Mark Anton dank der Taubenpost weiterhin mit seinen Verbündeten wie Aulus Hirtius kommunizieren und dadurch die Stadt vier Monate lang verteidigen konnte. Schon damals wurden die Nachrichten um die Füße der Brieftauben gebunden. Vor allem im 4. Jahrhundert wurde die Taubenpost im Römischen Reich stark ausgebaut, zeitweise waren bis zu 5000 Brieftauben in Staatsbesitz.

Auch in China und Indien wurde die Brieftaube schon früh zur Nachrichtenübertragung verwendet. China baute auf der Grundlage der Taubenpost ein ganzes Postwesen auf.

Taubenpost im Internetzeitalter[Bearbeiten | Quelltext bearbeiten]
Mit [RFC 1149 und RFC 2549](https://de.wikipedia.org/wiki/IPoAC) liegt zumindest in der Theorie ein (als Aprilscherz [https://www.ietf.org/rfc/rfc1149.txt] gedachter) Standard vor, wie das im Internet gebräuchliche Internet Protocol mithilfe von Brieftauben übertragen werden kann. Es blieb nicht nur bei der Theorie: Am 28. April 2001 wurde in Bergen ein Versuch durchgeführt, bei dem tatsächlich IP-Pakete übertragen wurden. Insgesamt wurden dabei vier Pakete übertragen, bei einer Laufzeit von ca. 1,5 Stunden. (Ping-Zeiten liegen normalerweise in Größenordnungen von Milli- bis Zehntelsekunden.)

