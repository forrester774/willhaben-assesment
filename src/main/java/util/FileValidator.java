package util;

import java.io.File;
import java.util.function.Function;

public class FileValidator implements Function<String, Boolean> {

    /**
     * Validates if a file exists and is not a directory given a filepath.
     *
     * @param filePath The path that points to a file.
     * @return {@link Boolean} true if a file is valid, false if it is not.
     */
    public Boolean apply(String filePath) {
        File f = new File(filePath);
        return f.exists() && !f.isDirectory();
    }
}
