package util;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CommandLineArgumentsUtil {

    private static String ERROR_MSG_FOR_WRONG_INPUT = "Invalid number of arguments! Please provide exactly 2 input parameters! See -h|-help|--help for more information!";

    private static Logger logger = LogManager.getLogger(CommandLineArgumentsUtil.class.getName());

    private static final List<String> HELP_OPTIONS = List.of("--help", "-h", "-help");

    /**
     * Validates the arguments given when starting the application. Displays help when necessary.
     * @param args
     * @return
     */
    public Pair<String, String> validateCommandLineArgs(String[] args) {
        int argsLength = args.length;
        if (argsLength == 0 || argsLength > 2) {
            logger.error(ERROR_MSG_FOR_WRONG_INPUT);
            displayHelp();
            return null;
        }

        if (argsLength == 1) {
            if (HELP_OPTIONS.contains(args[0])) {
                displayHelp();
                return null;
            }
            logger.error(ERROR_MSG_FOR_WRONG_INPUT);
            return null;
        }

        return getFilePathAndTargetHashes(args);
    }

    /**
     * Displays a help message that guides towards running the application correctly.
     */
    public void displayHelp() {
        logger.info("To run the applicaton, please provide two file paths: First provide the file path with the links," +
                "Then provide the file path with the target hashes. \n\n" +
                "Example: java -jar target/willhaben-assesment-exec.jar ABSOLUTE_PATH_FOR_LINKS_FILE ABSOLUTE_PATH_FOR_HASHES_FILE");
    }

    /**
     * Checks whether the input files exist and are actual files.
     * @param args
     * @return returns the pair of paths if both are valid. Return null if one or both files are invalid.
     */
    private Pair<String, String> getFilePathAndTargetHashes(String[] args) {
        String urlFilePath = args[0];
        String hashesFilePath = args[1];
        if (isFileValid(urlFilePath) && isFileValid(hashesFilePath)) {
            return Pair.of(urlFilePath, hashesFilePath);
        }
        return null;
    }

    // Public for testing purposes only, would use PowerMock or Reflection to mock this..
    /**
     * Checks whether the input file exist and is an actual file.
     * @param urlFilePath The files path.
     * @return returns false if the file path is invalid (not a file or file doesnt exist). Returns true otherwise.
     */
    public boolean isFileValid(String urlFilePath) {
        FileValidator fileValidator = new FileValidator();
        if (urlFilePath.isBlank() || !fileValidator.apply(urlFilePath)) {
            logger.error("The file " + urlFilePath + " is not valid! Please check the arguments used when running the application!");
            return false;
        }
        return true;
    }
}
