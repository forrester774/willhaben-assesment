package util;

import exception.FileParsingException;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkExtractor {

    private static final String VALID_URL_REGEX = "(https?|http)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    private static Logger logger = LogManager.getLogger(LinkExtractor.class.getName());

    /**
     * The method reads through a text file line by line and collects all links that match {@link VALID_URL_REGEX}
     * then returns them in an {@link ArrayList}.
     *
     * @param filePath The path for the file to be read.
     * @return ArrayList<String>. A collection of valid URLs.
     */
    public List<String> extractLinks(final String filePath) {

        Pattern pattern = Pattern.compile(VALID_URL_REGEX);
        List<String> validUrls = new ArrayList<>();
        UrlValidator urlValidator = new UrlValidator();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                addMatchesFromLineToResultSet(pattern, validUrls, urlValidator, currentLine);
            }
            reader.close();
        } catch (Exception exception) {
            logger.error("An exception occurred while parsing " + filePath + " text file!", exception);
            throw new FileParsingException("An exception occurred while parsing " + filePath + " text file!", exception);
        }
        return validUrls;
    }

    /**
     * Checks if a line has links. If it finds any links, validates them and adds them to the resultset.
     * @param pattern REGEX pattern for links.
     * @param validUrls The resultSet
     * @param urlValidator A URL validator class, that checks the validity of links with its isValid method.
     * @param currentLine The current line being read from the file.
     */
    private void addMatchesFromLineToResultSet(final Pattern pattern,
                                               final List<String> validUrls,
                                               final UrlValidator urlValidator,
                                               final String currentLine) {
        if (currentLine.length() > 0) {
            Matcher m = pattern.matcher(currentLine);
            while (m.find()) {
                String url = m.group();
                if (urlValidator.isValid(url))
                    validUrls.add(url);
            }
        }
    }
}
