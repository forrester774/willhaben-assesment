package util;

import exception.FileParsingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

public class HashesFileUtil {

    private static final int NUMBER_OF_CHARACTERS_IN_MD5_HASH_WITH_PADDING = 24;

    private static final String PADDING = "==";

    private static Logger logger = LogManager.getLogger(HashesFileUtil.class.getName());

    /**
     * Reads a file and reads its content. If the line is a correct hash, then adds it to the resultSet.
     * @param hashesFilePath The path of the file with the hashes in it.
     * @return returns a Set of hashes.
     */
    public Set<String> getHashesFromFile(String hashesFilePath) {

        Set<String> hashes = new HashSet<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(hashesFilePath));
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                if (validateHash(currentLine)) {
                    hashes.add(currentLine);
                }
            }
            reader.close();
        } catch (Exception exception) {
            logger.error("An exception occurred while parsing " + hashesFilePath + " text file!", exception);
            throw new FileParsingException("An exception occurred while parsing " + hashesFilePath + " text file!", exception);
        }

        return hashes;

    }

    /**
     * Checks if a line of a text file is an MD5 hash. (24 characters with padding, with '==' characters at the end.
     * @param currentLine
     * @return
     */
    private boolean validateHash(String currentLine) {
        return currentLine.length() == NUMBER_OF_CHARACTERS_IN_MD5_HASH_WITH_PADDING
                && currentLine.substring(NUMBER_OF_CHARACTERS_IN_MD5_HASH_WITH_PADDING - 2,
                NUMBER_OF_CHARACTERS_IN_MD5_HASH_WITH_PADDING).equals(PADDING);
    }
}