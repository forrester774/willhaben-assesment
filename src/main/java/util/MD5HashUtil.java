package util;

import model.LinkHashModel;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5HashUtil {

    private static final Logger logger = LogManager.getLogger(MD5HashUtil.class.getName());

    private static final String ALGORITHM = "MD5";

    /** Calculates the MD5 hashes of the webpage contents.
     * @param content Contents of a webpage in String form.
     * @param link The URL that the content is from.
     * @return returns a {@link LinkHashModel} with the link and the MD5 hash.
     */
    public LinkHashModel calculateHashAndBuildModel(String content, String link) {
        try {
            MessageDigest md = MessageDigest.getInstance(ALGORITHM);
            md.update(content.getBytes());

            String md5Hash = Base64.encodeBase64String(md.digest());

            return new LinkHashModel(link, md5Hash);
        } catch (NoSuchAlgorithmException exception) {
            logger.error("No such hashing algorithm as " + ALGORITHM, exception);
        }

        return null;
    }
}
