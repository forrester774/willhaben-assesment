package util;

import java.net.URI;
import java.util.Objects;
import java.util.function.Predicate;

public class ValidUrlPredicateBuilder {

    /**
     * The method creates a {@link Predicate<URI>} that defines a ruleset of what a 'valid' URI is and is not.
     *
     * @return returns a {@link Predicate<URI>}
     */
    public static Predicate<URI> build() {

        Predicate<URI> urlHasNoPort = uri -> uri.getPort() == -1;
        Predicate<URI> urlHasNoQuery = uri -> Objects.isNull(uri.getQuery());
        Predicate<URI> urlHasNoFragment = uri -> Objects.isNull(uri.getFragment()) && Objects.isNull(uri.getRawFragment());
        Predicate<URI> urlHasNoUserInfo = uri -> Objects.isNull(uri.getUserInfo()) && Objects.isNull(uri.getRawUserInfo());

        return urlHasNoPort
                .and(urlHasNoFragment)
                .and(urlHasNoQuery)
                .and(urlHasNoUserInfo);
    }
}
