package util;

import java.net.URI;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LinkValidator implements BiFunction<List<String>, Predicate<URI>, Queue<String>> {

    /**
     * The method filters all URIs that fail to adhere to conditions the Predicate provides.
     *
     * @param links        A collection of links to be filtered.
     * @param URIPredicate A {@link Predicate<URI>} with a defined ruleset.
     * @return returns a filtered collection of links in a {@link Queue<String>}.
     */
    @Override
    public Queue<String> apply(List<String> links, Predicate<URI> URIPredicate) {
        return links
                .stream()
                .map(URI::create)
                .filter(URIPredicate)
                .map(URI::toString)
                .collect(Collectors.toCollection(ConcurrentLinkedQueue::new));
    }
}
