import org.apache.commons.lang3.tuple.Pair;
import thread.URLFinderService;
import util.CommandLineArgumentsUtil;

import java.util.Objects;


public class Main {
    public static void main(String[] args) {

        CommandLineArgumentsUtil commandLineArgumentsUtil = new CommandLineArgumentsUtil();
        Pair<String, String> filePathPair = commandLineArgumentsUtil.validateCommandLineArgs(args);

        if (Objects.isNull(filePathPair)) {
            return;
        }

        URLFinderService urlFinderService = new URLFinderService();
        urlFinderService.findUrlsForTargetHashes(filePathPair);
    }
}
