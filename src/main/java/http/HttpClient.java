package http;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class HttpClient {

    private final Logger logger = LogManager.getLogger(HttpClient.class.getName());

    private final CloseableHttpClient httpClient;

    private static final int numberOfRetriesAllowed = 1;

    public HttpClient() {
        httpClient = HttpClients
                .custom()
                .setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build())
                .build();
    }

    /** Calls the link with http get and gets the content. Upon error, it retries once more.
     * @param link
     * @return
     */
    public Optional<String> callHttpEndpoint(String link) {
        HttpGet request = new HttpGet(link);
        int retries = 0;
        while (retries <= numberOfRetriesAllowed) {
            try {
                CloseableHttpResponse response = httpClient.execute(request);

                HttpEntity entity = response.getEntity();
                String webpageContent = EntityUtils.toString(entity);

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode >= 200 && statusCode < 400 && !webpageContent.isBlank()) {
                    return Optional.of(webpageContent);
                }
            } catch (Exception exception) {
                logger.error("An error occurred while attempting to call endpoint with HTTP GET: " + link, exception);
                retries++;
            }
        }
        return Optional.empty();
    }
}
