package thread;

import http.HttpClient;
import model.LinkHashModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import targethashhandler.TargetHashHandler;
import targethashhandler.impl.TargetHashHandlerImpl;
import util.MD5HashUtil;

import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;


public class LinkProcessorThread extends Thread {

    private static Logger logger = LogManager.getLogger(LinkProcessorThread.class.getName());

    private Queue<String> queue;
    private Set<String> targetHashes;
    private boolean exit;

    public LinkProcessorThread(Queue<String> queue, Set<String> targetHashes) {
        this.queue = queue;
        this.targetHashes = targetHashes;
    }

    /**
     * Runs the process for each thread. Gets a link from the Queue, gets the content with HTTP GET with {@link HttpClient},
     * then calculates the MD5 hash values of the content with {@link MD5HashUtil} and then prints the link and its hash if
     * the targetHashes Set contains said hash.
     */
    @Override
    public void run() {
        HttpClient httpClient = new HttpClient();
        TargetHashHandler targetHashHandler = new TargetHashHandlerImpl();
        MD5HashUtil md5HashUtil = new MD5HashUtil();
        while (!exit) {
            String link = queue.poll();

            if (Objects.isNull(link)) {
                logger.debug("The Queue is empty Thread" + this.toString() + " is stopping..");
                exit = true;
            } else {
                Optional<String> content = httpClient.callHttpEndpoint(link);
                if (content.isPresent()) {
                    LinkHashModel linkHashModel = md5HashUtil.calculateHashAndBuildModel(content.get(), link);
                    if (Objects.nonNull(linkHashModel)) {
                        targetHashHandler.handleTargetHash(linkHashModel, targetHashes);
                    }
                }
            }
        }
    }


}
