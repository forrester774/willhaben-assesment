package thread;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import util.HashesFileUtil;
import util.LinkExtractor;
import util.LinkValidator;
import util.ValidUrlPredicateBuilder;

import java.util.List;
import java.util.Queue;
import java.util.Set;

public class URLFinderService {

    private static Logger logger = LogManager.getLogger(URLFinderService.class.getName());

    /**
     * Extracts links and hashes from each of their files, creating 2 collections.
     * Validates the links with {@link LinkValidator} and then processes these links and hashes in a parallel way.
     *
     * @param pairOfFilePaths
     */
    public void findUrlsForTargetHashes(Pair<String, String> pairOfFilePaths) {

        String linkFilePath = pairOfFilePaths.getLeft();
        LinkExtractor linkExtractor = new LinkExtractor();
        List<String> extractedLinks = linkExtractor.extractLinks(linkFilePath);

        String hashesFilePath = pairOfFilePaths.getRight();
        HashesFileUtil hashesFileUtil = new HashesFileUtil();
        Set<String> hashesFromFile = hashesFileUtil.getHashesFromFile(hashesFilePath);

        if (extractedLinks.isEmpty()) {
            logger.error("There are no valid links in this file! File: " + linkFilePath);
            return;
        }

        if (hashesFromFile.isEmpty()) {
            logger.error("There are no hashes in the file! File: " + hashesFilePath);
            return;
        }

        LinkValidator linkValidator = new LinkValidator();

        Queue<String> filteredLinks = linkValidator.apply(extractedLinks, ValidUrlPredicateBuilder.build());

        findUrlsForTargetHashes(hashesFromFile, filteredLinks);
    }

    /**
     * Creates cores for each processor in the computer.
     * @param hashesFromFile
     * @param filteredLinks
     */
    private void findUrlsForTargetHashes(Set<String> hashesFromFile, Queue<String> filteredLinks) {
        int cores = Runtime.getRuntime().availableProcessors();
        for (int i = 0; i < cores; i++) {
            LinkProcessorThread thread = new LinkProcessorThread(filteredLinks, hashesFromFile);
            thread.start();
        }
    }
}
