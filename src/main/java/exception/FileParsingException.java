package exception;


public class FileParsingException extends RuntimeException {

    public FileParsingException(String message, Exception cause) {
        super(message, cause);
    }
}
