package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LinkHashModel {
    private String link;
    private String md5Hash;

    @Override
    public String toString() {
        return "Link found for target hash: " + md5Hash + " -> Link: " + link;
    }
}
