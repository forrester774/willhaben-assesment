package targethashhandler;

import model.LinkHashModel;

import java.util.Set;

public interface TargetHashHandler {

    void handleTargetHash(LinkHashModel linkHashModel, Set<String> targetHashes);
}
