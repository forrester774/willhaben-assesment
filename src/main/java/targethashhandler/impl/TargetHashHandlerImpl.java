package targethashhandler.impl;

import model.LinkHashModel;
import targethashhandler.TargetHashHandler;

import java.util.Set;

public class TargetHashHandlerImpl implements TargetHashHandler {

    /**
     * Prints the link and its md5 hash if the hash if found in the targetHashes collection.
     *
     * @param linkHashModel A class with a link and hash fields.
     * @param targetHashes  Collection of md5 hashes.
     */
    @Override
    public void handleTargetHash(LinkHashModel linkHashModel, Set<String> targetHashes) {
        if (targetHashes.contains(linkHashModel.getMd5Hash())) {
            System.out.println(linkHashModel.toString());
        }
    }
}
