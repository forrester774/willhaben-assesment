package util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class HashesFileUtilTest {

    private static String testPath = "src/test/java/util/files/testHashes";
    private static String incorrectFilePath = "src/test/java/util/files/incorrectHashes";

    private HashesFileUtil hashesFileUtil;

    @BeforeEach
    public void setup() {
        hashesFileUtil = new HashesFileUtil();
    }

    @Test
    @DisplayName("Run with correct hashFile -> returns all valid hashes")
    public void testRunWithCorrectHashes() {
        Set<String> hashesFromFile = hashesFileUtil.getHashesFromFile(testPath);
        Assertions.assertEquals(2, hashesFromFile.size());
    }

    @Test
    @DisplayName("Run with correct hashFile -> returns all valid hashes")
    public void testRunWithInCorrectHashes() {
        Set<String> hashesFromFile = hashesFileUtil.getHashesFromFile(incorrectFilePath);
        Assertions.assertEquals(0, hashesFromFile.size());
    }
}
