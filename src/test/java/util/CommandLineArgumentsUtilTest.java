package util;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class CommandLineArgumentsUtilTest {

    CommandLineArgumentsUtil commandLineArgumentsUtil;

    @BeforeEach
    public void setup() {
        commandLineArgumentsUtil = spy(new CommandLineArgumentsUtil());
    }

    @Test
    @DisplayName("Run App with 0 arguments -> returns null")
    public void testRunWithZeroArguments() {
        Pair<String, String> result = commandLineArgumentsUtil.validateCommandLineArgs(new String[0]);
        Mockito.verify(commandLineArgumentsUtil, Mockito.times(1)).displayHelp();
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Run App with 1 argument that is invalid -> error -> returns null")
    public void testRunWithOneInvalidArgument() {
        String[] args = {"invalid"};
        Pair<String, String> result = commandLineArgumentsUtil.validateCommandLineArgs(args);
        Mockito.verify(commandLineArgumentsUtil, Mockito.times(0)).displayHelp();
        Assertions.assertNull(result);
    }


    @Test
    @DisplayName("Run App with 1 argument that is valid -> displays help -> returns null")
    public void testRunWithOneValidArgument() {
        String[] args = {"--help"};
        Pair<String, String> result = commandLineArgumentsUtil.validateCommandLineArgs(args);
        Mockito.verify(commandLineArgumentsUtil, Mockito.times(1)).displayHelp();
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Run App with 2 valid arguments -> returns valid Pair")
    public void testRunWithTwoValidArguments() {
        String[] args = {"file1", "file2"};

        doReturn(Boolean.TRUE).when(commandLineArgumentsUtil).isFileValid(any());
        Pair<String, String> result = commandLineArgumentsUtil.validateCommandLineArgs(args);
        Mockito.verify(commandLineArgumentsUtil, Mockito.times(0)).displayHelp();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(args[0], result.getLeft());
        Assertions.assertEquals(args[1], result.getRight());
    }

    @Test
    @DisplayName("Run App with 2 valid arguments -> returns valid Pair")
    public void testRunWithTwoInvalidArguments() {
        String[] args = {"file1", "file2"};

        doReturn(Boolean.FALSE).when(commandLineArgumentsUtil).isFileValid(any());
        Pair<String, String> result = commandLineArgumentsUtil.validateCommandLineArgs(args);
        Mockito.verify(commandLineArgumentsUtil, Mockito.times(0)).displayHelp();

        Assertions.assertNull(result);
    }

}
