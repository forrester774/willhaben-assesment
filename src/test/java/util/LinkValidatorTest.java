package util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Queue;

public class LinkValidatorTest {

    @Test
    @DisplayName("test LinkValidator and the ValidUrlPredicateBuilder with a mix of valid and invalid links -> returns the 2 valid links from the collection.")
    public void testLinkValidator() {
        LinkValidator linkValidator = new LinkValidator();
        List<String> links = List.of("https://me:secret@test.com/wh-assessment-101/invalid.txt",
                "https://test.com/wh/fragment#invalid",
                "https://test.com/wh-101/query?invalid=true",
                "https://test.com:8080/wh-101",
                "https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force",
                "https://tech.willhaben.at/");

        Queue<String> result = linkValidator.apply(links, ValidUrlPredicateBuilder.build());

        Assertions.assertEquals(2, result.size());
    }
}
