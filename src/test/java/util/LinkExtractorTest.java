package util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

public class LinkExtractorTest {

    private LinkExtractor linkExtractor;

    private final String fileWithValidLinks = "src/test/java/util/files/testText";
    private final String fileWithInvalidLinks = "src/test/java/util/files/incorrectLinksText";

    @BeforeEach
    public void setup() {
        linkExtractor = new LinkExtractor();
    }

    @Test
    @DisplayName("Run test with all valid links -> Gets all (5) links")
    public void testLinkExtractorWithValidLinks() {
        List<String> links = linkExtractor.extractLinks(fileWithValidLinks);
        Assertions.assertEquals(5, links.size());
    }

    @Test
    @DisplayName("Run test with all but 1 invalid links (All wikipedia links are not valid anymore in this file) -> Gets 1 link")
    public void testLinkExtractorWithInvalidLinks() {
        List<String> links = linkExtractor.extractLinks(fileWithInvalidLinks);
        Assertions.assertEquals(1, links.size());
    }

}
