package util;

import model.LinkHashModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MD5HashUtilTest {

    @Test
    @DisplayName("Test MD5HashUtil with the target hashes -> the contents MD5 hashes are the same as expected")
    public void testMD5HashUtil() throws IOException {
        MD5HashUtil hashUtil = new MD5HashUtil();
        String rfc1149Text = new String(Files.readAllBytes(Paths.get("src/test/java/util/files/rfc1149.txt")));
        String rfc1149Link = "https://www.ietf.org/rfc/rfc1149.txt";
        LinkHashModel rfc1149 = hashUtil.calculateHashAndBuildModel(rfc1149Text, rfc1149Link);
        Assertions.assertEquals("5zAjHAcCDH/HsNXfEoVeMA==", rfc1149.getMd5Hash());
        Assertions.assertEquals(rfc1149Link, rfc1149.getLink());

        String rfc3986Text = new String(Files.readAllBytes(Paths.get("src/test/java/util/files/rfc3986.txt")));
        String rfc3986Link = "https://tools.ietf.org/rfc/rfc3986.txt";
        LinkHashModel rfc3986 = hashUtil.calculateHashAndBuildModel(rfc3986Text, rfc3986Link);
        Assertions.assertEquals("RbwRYKKAw0uSMwmukf8oOg==", rfc3986.getMd5Hash());
        Assertions.assertEquals(rfc3986Link, rfc3986.getLink());
    }

}
