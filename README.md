Willhaben assesment

Needs Java version 11.

1. To build the application and to run the tests, invoke the `mvn clean install` command.
2. An executable jar will be created called willhaben-assesment-exec.jar.
3. To run the program, run the following command with 2 arguments:
   ` java -jar target/willhaben-assesment-exec.jar` ABSOLUTE_PATH_FOR_LINKS_FILE ABSOLUTE_PATH_FOR_HASHES_FILE
4. The two input parameters are absolute paths to files with the links in them and the target hashes.
5. The application will print the links with the target hashes at the end of the process.
